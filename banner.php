<!-- Inicio sección banner -->





<div class="container">
    <section class="pasos margin-tb">
        <h3 class="titulos-secciones centrar-texto mayusculas">¿Cómo funciona la plataforma?</h3>
        <div class="row">
            <div class="col-lg-6">
                <picture style="">
                    <source class="centrar-imagen-V" loading="lazy" srcset="img/index/banner/pasitos.webp" type"image/webp">
                    <img class="centrar-imagen-V" loading="lazy" src="img/index/banner/pasitos.webp" alt="Imagen" />
                </picture>
            </div>
            <div class="col-lg-6">

                <ul class="list-group">
                    <span class="badge badge-primary badge-pill">1</span>
                    <li class="list-group-item justify-content-between align-items-center">
                        <h4>Registrarse en la plataforma</h4>
                        <p>Se crea una cuenta gratuita en cuestión de minutos con nuestro increíble proceso de registro.</p>
                    </li>
                    <span class="badge badge-primary badge-pill">2</span>
                    <li class="list-group-item  justify-content-between align-items-center">
                        <h4>Indica si eres un alumno o transcriptor</h4>
                        <p>Es importante identificar tu papel para redirigirte al sitio que te ofrezca las herramientas que se ajusten a tus necesidades.</p>

                    </li>
                    <span class="badge badge-primary badge-pill">3</span>
                    <li class="list-group-item  justify-content-between align-items-center">
                        <h4>Solicitar un transcriptor</h4>
                        <p>Los alumnos llegan a un acuerdo con los transcriptores y viceversa para proceder con el envío de los archivos a transcribir.</p>

                    </li>
                    <span class="badge badge-primary badge-pill">4</span>
                    <li class="list-group-item  justify-content-between align-items-center">
                        <h4>Realiza el intercambio.</h4>
                        <p>El alumno recibirá su archivo transcrito y el transcriptor su pago</p>

                    </li>
                </ul>

            </div>
        </div>
    </section>

    <section class="sobrenostros margin-tb" id="sn">
        <div class="separador"></div>
        <h3 class="titulos-secciones centrar-texto mayusculas">Sobre nosotros: </h3>
        <p>Nuestro equipo se conforma de personas con diferentes hebilidades que contribuyen a mejorar el
            desarrollo y funcionamiento de la plataforma.

        </p>

        <div class="col-12 col-md-6 col-lg-3">
            <div class="card border-light mb-4 animate-up-5"><a href=".https://geramh.com" class="position-relative"><img src="img/developers/GMH.jpg" class="card-img-top p-2 rounded-xl" alt="themesberg office"></a>
                <div class="card-body"><a href="https://geramh.com">
                        <h4 class="" style="color: black;">Gerardo MH</h4>
                    </a>
                    <ul class="list-group mb-3">
                        <li class="list-group-item small p-0"> <span class="iconify" data-icon="bi:patch-check" data-inline="false"></span> Estudiante</li>
                        <li class="list-group-item small p-0"> <span class="iconify" data-icon="bi:patch-check" data-inline="false"></span> Desarrollador de Video Juegos</li>
                        <li class="list-group-item small p-0"> <span class="iconify" data-icon="bi:patch-check" data-inline="false"></span> Desarrollador Back-end</li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3">
            <div class="card border-light mb-4 animate-up-5"><a href="#" class="position-relative"><img src="img/developers/AFB.jpg" class="card-img-top p-2 rounded-xl" alt="themesberg office"></a>
                <div class="card-body"><a href="#">
                        <h4 class="" style="color: black;">Abraham FB</h4>
                    </a>

                    <ul class="list-group mb-3">
                        <li class="list-group-item small p-0"> <span class="iconify" data-icon="bi:patch-check" data-inline="false"></span> Estudiante</li>
                        <li class="list-group-item small p-0"> <span class="iconify" data-icon="bi:patch-check" data-inline="false"></span> Desarrollador Front-end</li>
                        <li class="list-group-item small p-0"> <span class="iconify" data-icon="bi:patch-check" data-inline="false"></span> Técnico en Redes CISCO</li>
                        <li class="list-group-item small p-0"> <span class="iconify" data-icon="bi:patch-check" data-inline="false"></span> Freelancer</li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3">
            <div class="card border-light mb-4 animate-up-5"><a href="#" class="position-relative"><img src="img/developers/LOM.jpg" class="card-img-top p-2 rounded-xl" alt="themesberg office"></a>
                <div class="card-body"><a href="#">
                        <h4 class="" style="color: black;">Lizbeth OM</h4>
                    </a>
                    <ul class="list-group mb-3">
                        <li class="list-group-item small p-0"> <span class="iconify" data-icon="bi:patch-check" data-inline="false"></span> Estudiante</li>
                        <li class="list-group-item small p-0"> <span class="iconify" data-icon="bi:patch-check" data-inline="false"></span> Administradora de Bases de Datos</li>
                        <li class="list-group-item small p-0"> <span class="iconify" data-icon="bi:patch-check" data-inline="false"></span> Desarrollador Front-end</li>
                    </ul>
                </div>

            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-3">
            <div class="card border-light mb-4 animate-up-5"><a href="#" class="position-relative"><img src="img/developers/AGV.jpg" class="card-img-top p-2 rounded-xl" alt="themesberg office"></a>
                <div class="card-body"><a href="#">
                        <h4 class="" style="color: black;">Ángel GV</h4>
                    </a>
                    <ul class="list-group mb-3">
                        <li class="list-group-item small p-0"> <span class="iconify" data-icon="bi:patch-check" data-inline="false"></span> Estudiante</li>
                        <li class="list-group-item small p-0"> <span class="iconify" data-icon="bi:patch-check" data-inline="false"></span> Diseñador gráfico</li>
                    </ul>
                </div>

            </div>
        </div>
    </section>


    <section id="contacto" class="margin-tb mayusculas">
        <div class="info-contacto">
            <div class="separador"></div>
            <h3 class="titulos-secciones centrar-texto">Contáctanos</h3>
            <p>Correo: <span>contacto@cv.com</span></p>
            <p>Teléfono <span>2217364821</span></p>
        </div>
    </section>

</div>