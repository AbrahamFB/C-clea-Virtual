<!-- Inicio sección footer-->


<footer class="new_footer_area bg_color">
    <div class="new_footer_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="f_widget company_widget wow fadeInLeft" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInLeft;">
                        <h3 class="f-title f_600 t_color f_size_18">Sobre Nosotros</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, nemo beatae. Necessitatibus, cum cupiditate aliquam consequuntur a, blanditiis est nam neque alias error officiis nemo deserunt illo repellendus voluptatum quidem?</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="f_widget about-widget pl_70 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <h3 class="f-title f_600 t_color f_size_18">Conoce más</h3>
                        <ul class="list-unstyled f_list">
                            <li><a href="https://www.gob.mx/conadis/articulos/lengua-de-senas-mexicana-lsm?idiom=es">Qué es el LSM.</a></li>
                            <li><a href="https://celeidiomas.com.mx/diplomado-en-lengua-de-senas-mexicanas/">Consigue tu certificado de LSM.</a></li>
                            <li><a href="https://gala-dev.com">Nuestros servicios.</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="f_widget about-widget pl_70 wow fadeInLeft" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                        <h3 class="f-title f_600 t_color f_size_18">Ayuda</h3>
                        <ul class="list-unstyled f_list">
                            <li><a href="https://www.gob.mx/conadis/archivo/prensa">Preguntas Frecuentes</a></li>
                            <li><a href="https://correobuap-my.sharepoint.com/:f:/g/personal/abraham_floresb_alumno_buap_mx/ElJ-jAFBqQhGkpzQ-N7rT8QBLLtRcl_OfjT6_HVrntTgMQ?e=ytuTv9">Documentación</a></li>
                            <li><a href="privacidad.php">Privacidad</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="f_widget social-widget pl_70 wow fadeInLeft" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInLeft;">
                        <h3 class="f-title f_600 t_color f_size_18">Equipo de Soporte</h3>
                        <div class="f_social_icon">
                            <a href="https://geramh.com/"><span class="iconify" data-icon="noto:boy-light-skin-tone" data-inline="false"></span></a>
                            <a href="https://abrahamfb.com"><span class="iconify" data-icon="noto:boy-light-skin-tone" data-inline="false"></span></a>
                            <a href="https://beth27.com/"><span class="iconify" data-icon="noto:girl-light-skin-tone" data-inline="false"></span></a>
                            <a href="#"><span class="iconify" data-icon="noto:boy-light-skin-tone" data-inline="false"></span></a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bg">
            <div class="footer_bg_one"></div>
            <div class="footer_bg_two"></div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-sm-7">
                    <p class="mb-0 f_400">
                    <div class="copyright">
                        ©
                        <script>
                            document.write(new Date().getFullYear());
                        </script>

                        <strong class="logo-inicio">Cóclea Virtual</strong>. Todos los Derechos
                        Reservados

                    </div>
                    </p>
                </div>
                <div class="col-lg-6 col-sm-5 text-right">
                    <p>Desarrollado <i class="icon_heart"></i> por <a href="https://gala-dev.com">GALA-Dev</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Fin sección footer -->