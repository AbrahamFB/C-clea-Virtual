<?php
$anadirURL = "";
$nombrePagina = "Cóclea Virtual";
$headerPagina = '<h1>Soy <span class="typed"></span></h1>
            <p>
                Estudiante, Desarrollador, Técnico en redes-CISCO, Freelancer,
                Fotógrafo
            </p>';
$css_extra = "";

session_start(); //Iniciar sesión

include("html.php");

echo '<body ondragstart="return false">';

include("nav-bar_index.php");


?>

<div class="container">
    <h1 class="centrar-texto mayusculas">POLÍTICA DE PRIVACIDAD</h1>
    <br>
    <li class="list-group-item">
        <p><span class="iconify" data-icon="akar-icons:circle-check" data-inline="false"></span> La presente Política de Privacidad establece los términos en que se usan y protegen la información que es proporcionada por nuestros usuarios al momento de utilizar nuestro sitio web. Esta compañía está comprometida con la seguridad de los datos de sus usuarios. Cuando le pedimos llenar los campos de información personal con la cual usted pueda ser identificado, lo hacemos asegurando que sólo se empleará de acuerdo con los términos de este documento. Sin embargo esta Política de Privacidad puede cambiar con el tiempo o ser actualizada por lo que le recomendamos y enfatizamos revisar continuamente esta página para asegurarse que está de acuerdo con dichos cambios.</p>
    </li>
    <p class="negritas">Información que es recogida</p>
    <li class="list-group-item">
        <p><span class="iconify" data-icon="akar-icons:circle-check" data-inline="false"></span> Nuestro sitio web podrá recoger información personal por ejemplo: Nombre, información de contacto como su dirección de correo electrónica e información demográfica. Así mismo cuando sea necesario podrá ser requerida información específica para procesar algún pedido o realizar una entrega o facturación</p>
    </li>
    <p class="negritas">Uso de la información recogida</p>
    <li class="list-group-item">
        <p><span class="iconify" data-icon="akar-icons:circle-check" data-inline="false"></span> Nuestro sitio web emplea la información con el fin de proporcionar el mejor servicio posible, particularmente para mantener un registro de usuarios, de pedidos en caso que aplique, y mejorar nuestros productos y servicios. Es posible que sean enviados correos electrónicos periódicamente a través de nuestro sitio con ofertas especiales, nuevos productos y otra información publicitaria que consideremos relevante para usted o que pueda brindarle algún beneficio, estos correos electrónicos serán enviados a la dirección que usted proporcione y podrán ser cancelados en cualquier momento.</p>
    </li>
    <li class="list-group-item">
        <p><span class="iconify" data-icon="akar-icons:circle-check" data-inline="false"></span> <span class="negritas">Cóclea Vrtual</span> está altamente comprometido para cumplir con el compromiso de mantener su información segura. Usamos los sistemas más avanzados y los actualizamos constantemente para asegurarnos que no exista ningún acceso no autorizado.</p>
    </li>
    <p class="negritas">Cookies</p>
    <li class="list-group-item">
        <p><span class="iconify" data-icon="akar-icons:circle-check" data-inline="false"></span> Una cookie se refiere a un fichero que es enviado con la finalidad de solicitar permiso para almacenarse en su ordenador, al aceptar dicho fichero se crea y la cookie sirve entonces para tener información respecto al tráfico web, y también facilita las futuras celta de vigo noticias recurrente. Otra función que tienen las cookies es que con ellas las web pueden reconocerte individualmente y por tanto brindarte el mejor servicio personalizado de su web.</p>
    </li>
    <li class="list-group-item">
        <p><span class="iconify" data-icon="akar-icons:circle-check" data-inline="false"></span> Nuestro sitio web emplea las cookies para poder identificar las páginas que son visitadas y su frecuencia. Esta información es empleada únicamente para análisis estadístico y después la información se elimina de forma permanente. Usted puede eliminar las cookies en cualquier momento desde su ordenador. Sin embargo las cookies ayudan a proporcionar un mejor servicio de los sitios web, estás no dan acceso a información de su ordenador ni de usted, a menos de que usted así lo quiera y la proporcione directamente. Usted puede aceptar o negar el uso de cookies, sin embargo la mayoría de navegadores aceptan cookies automáticamente pues sirve para tener un mejor servicio web. También usted puede cambiar la configuración de su ordenador para declinar las cookies. Si se declinan es posible que no pueda utilizar algunos de nuestros servicios.</p>
    </li>
    <p class="negritas">Enlaces a Terceros</p>
    <li class="list-group-item">
        <p><span class="iconify" data-icon="akar-icons:circle-check" data-inline="false"></span> Este sitio web pudiera contener enlaces a otros sitios que pudieran ser de su interés. Una vez que usted de clic en estos enlaces y abandone nuestra página, ya no tenemos control sobre al sitio al que es redirigido y por lo tanto no somos responsables de los términos o privacidad ni de la protección de sus datos en esos otros sitios terceros. Dichos sitios están sujetos a sus propias políticas de privacidad por lo cual es recomendable que los consulte para confirmar que usted está de acuerdo con estas.</p>
    </li>
    <p class="negritas">Control de su información personal</p>

    <li class="list-group-item">
        <p><span class="iconify" data-icon="akar-icons:circle-check" data-inline="false"></span> Esta compañía no venderá, cederá ni distribuirá la información personal que es recopilada sin su consentimiento, salvo que sea requerido por un juez con un orden judicial.</p>
    </li>
    <li class="list-group-item">
        <p><span class="iconify" data-icon="akar-icons:circle-check" data-inline="false"></span> <span class="negritas">Cóclea Vrtual</span> se reserva el derecho de cambiar los términos de la presente Política de Privacidad en cualquier momento.</p>
    </li>

</div>
<br>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<?php
include("footer.php");

include("scripts.php");

echo '  </body>
</html>';
