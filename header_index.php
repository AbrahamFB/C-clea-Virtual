<!-- Inicio sección header -->
<section class="site-header">


    <div class="hero">
        <div class="efectito" style="background: #0069ff; background: linear-gradient(45deg,#0069ff 0%,#ff3a6f 100%);  opacity: 0.5; "></div>

        <div class="container">

            <div class="row">
                <div class="col-lg-6 contenido-header">
                    <h2 class="presentacion">Te transcribimos cualquier audio o vídeo</h2>
                    <div class="w-100 d-none d-md-block"></div>

                </div>
                <div class="col-lg-6 contenido-header">
                    <div class="row row-cols-4">
                        <div class="col-md-6">
                            <span class="iconify" data-icon="ion:laptop-outline" data-inline="false"></span>
                            <h4>Accede desde cualquier dispositivo</h4>
                            <p class="descripcion-header">Aprovechamos las nuevas tecnologías para dar mayor accesibilidad a la aplicación web.</p>
                        </div>
                        <div class="col-md-6">
                            <span class="iconify" data-icon="ion:ear-outline" data-inline="false"></span>
                            <h4>Dirigido para las personas con alguna discapacidad auditiva.</h4>
                            <p class="descripcion-header">Nuestro objetivo es visibilizar a la población de discapacitados y facilitar la comunicación con ellos.</p>
                        </div>
                        <div class="col-md-6">
                            <span class="iconify" data-icon="ion:folder-open-outline" data-inline="false"></span>
                            <h4>Admitimos cualquier archivo multimedia</h4>
                            <p class="descripcion-header">Desde documentos en cualquier formato, hasta audios y videos.</p>
                        </div>
                        <div class="col-md-6">
                            <span class="iconify" data-icon="ion:id-card-outline" data-inline="false"></span>
                            <h4>Generamos oportunidades de empleo.</h4>
                            <p class="descripcion-header">Dada la situación de la pandemia, queremos aprovechar las habilidades infravaloradles de LSM para generar empleos.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- Fin sección header -->